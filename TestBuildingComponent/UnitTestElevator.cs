using BuildingComponent;

namespace TestBuildingComponent
{
    [TestClass]
    public class UnitTestElevator
    {
        [TestMethod]
        public void TestRequest()
        {
            int expected = 2;
            Elevator elev = new Elevator(1);
            elev.Request(2);
            int result = (int)elev.Requests[0];
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMove()
        {
            string expected = "Moving up to Floor 3.";
            Elevator elev = new Elevator(1);
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);
                elev.Request(3);
                elev.Move(Elevator.Direction.Up);
                var result = sw.ToString().Trim();
                Assert.AreEqual(expected, result);
            }
        }
    }
}