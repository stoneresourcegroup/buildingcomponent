﻿namespace BuildingComponent
{
    public class Program
    {
        public static void Main()
        {
            Elevator elevator = new Elevator(5);
            elevator.Request(3);
            elevator.Request(1);
            elevator.Request(5);
            elevator.Request(2);

            while (elevator.Requests.Count > 0)
            {
                elevator.Move(Elevator.Direction.Down);
            }
        }
    }
}
