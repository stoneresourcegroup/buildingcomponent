﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingComponent
{
 
    public class Elevator
    {
        private int currentFloor;
        private int destinationFloor;
        private List<int> requests = new List<int>();

        public enum Direction
        {
            Up, 
            Down
        }

        public Elevator(int CurrentFloor)
        {
            this.currentFloor = CurrentFloor;
        }

        public List<int> Requests
        {
            get { return requests; }
        }

        public void Request(int floor)
        {
            requests.Add(floor);
         }

        public void Move(Direction dir)
        {
            if (dir == Direction.Up)
            {
                requests = requests.Distinct().OrderBy(x => x).ToList();
            }
            else
            {
                requests = requests.Distinct().OrderByDescending(x => x).ToList();
            }

            if (requests.Count == 0)
            {
                Console.WriteLine("Elevator is idle.");
                return;
            }

            int destinationFloor = requests[0];
            if (destinationFloor > currentFloor)
            {
                Console.WriteLine($"Moving up to Floor {destinationFloor}.");
            }
            else if (destinationFloor < currentFloor)
            {
                Console.WriteLine($"Moving down to Floor {destinationFloor}.");
            }
            else
            {
                Console.WriteLine($"Elevator has reached the requested floor, Floor { destinationFloor}.");
                requests.RemoveAt(0);
            }

            currentFloor = destinationFloor;
        }

    }
}
